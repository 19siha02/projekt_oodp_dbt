CREATE TABLE users
(
 user_id serial NOT NULL,
 first_name character varying(50),
 last_name character varying(50),
 password character varying(30),
 email character varying(30),
 PRIMARY KEY (user_id),
 CONSTRAINT emailunique UNIQUE (email),
 CONSTRAINT emailchk CHECK (email like '%_@%__.__%'),
 CONSTRAINT passwdChk CHECK (char_length(password) > 8)
); 
CREATE TABLE activity_categories
(
 category_id serial NOT NULL,
 category character varying(50),
 difficulty integer,
 PRIMARY KEY (category_id)
);
 CREATE TABLE activities
(
 activity_id bigserial NOT NULL,
 name character varying(50),
 location character varying(50),
 user_id integer NOT NULL,
 PRIMARY KEY (activity_id),
 FOREIGN KEY (user_id)
 REFERENCES users (user_id)
 ON UPDATE CASCADE
 ON DELETE CASCADE
 );
 CREATE TABLE points
(
 point_id serial NOT NULL,
 date date,
 "time" time,
 elapsed_time integer,
 latitude real,
 longitude real, 
 altitude real,
 distance real,
 heart_rate integer,
 speed real,
 cadence integer,
 activity_id integer NOT NULL,
 PRIMARY KEY (point_id),
 FOREIGN KEY(activity_id)
 REFERENCES activities (activity_id)
 ON UPDATE CASCADE
 ON DELETE CASCADE
);
 CREATE TABLE following
(
 follower integer NOT NULL,
 following integer NOT NULL,
 PRIMARY KEY (follower, following),
 FOREIGN KEY (follower)
 REFERENCES users(user_id)
 ON UPDATE CASCADE
 ON DELETE CASCADE,
 FOREIGN KEY (following)
 REFERENCES users(user_id)
 ON UPDATE CASCADE
 ON DELETE CASCADE

); 
