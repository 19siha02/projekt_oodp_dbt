package view;

import java.awt.Dimension;
import javax.swing.*;

import controller.Controller;

public class ActivityView extends JPanel {
	Controller controller = Controller.getInstance();
	String[] stats;
	JTabbedPane tabbedPane;

	public ActivityView(int activityId, int userId) {
		tabbedPane = new JTabbedPane();
		stats = controller.getStats(activityId, userId);
		if (stats != null) {
			this.add(tabbedPane);
			tabbedPane.setPreferredSize(new Dimension(1800, 500));


			tabbedPane.add("All", createAllPanel(activityId,userId));
			tabbedPane.add("HR", new PlotView("Heart Rate", controller.getActivityGraphData(userId, activityId, 0),
					controller.getActivityGraphTime(userId, activityId)));
			tabbedPane.add("SP", new PlotView("Speed", controller.getActivityGraphData(userId, activityId, 1),
					controller.getActivityGraphTime(userId, activityId)));
			tabbedPane.add("Alt", new PlotView("Altitude", controller.getActivityGraphData(userId, activityId, 2),
					controller.getActivityGraphTime(userId, activityId)));
			tabbedPane.add("Cad", new PlotView("Cadence", controller.getActivityGraphData(userId, activityId, 3),
					controller.getActivityGraphTime(userId, activityId)));

		}
	}

	public JPanel createAllPanel(int activityId, int userId) {

		JPanel panel = new JPanel();
		panel.setSize(800, 800);
		JTable table = new JTable(2, 20);
		String[] labels = { "Start", "End", "Duration", "Distance", "Min HR", "Max Hr", "Avg Hr", "Min Speed",
				"Max Speed", "Avg Speed", "Min Alt", "Max Alt", "Avg Alt", "Min Incl", "Max Incl", "Avg Incl",
				"Min Cad", "Max Cad", "Avg Cad" };
		for (int i = 0; i < stats.length; i++) {
			table.setValueAt(labels[i], 0, i);
			table.setValueAt(stats[i], 1, i);
		}
		table.setValueAt("Calories", 0, 19);
		//table.setValueAt(Integer.toString((int)controller.calcCalories(activityId,userId)) + "kCal", 1, 19);
		table.setValueAt(String.format("%.1f kCal", controller.calcCalories(activityId,userId)), 1, 19);
		
		panel.add(table);
		;
		return panel;

	}

}
