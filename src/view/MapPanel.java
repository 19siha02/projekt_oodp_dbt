package view;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Polygon;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import dao.PointsDAO;
import model.Point;

public class MapPanel extends JPanel {

	Polygon polygon = new Polygon();
	ImageIcon imageIcon;
	Image image;
	List<Point> list;
	double latDiff;
	double longDiff;
	double minLong;
	double maxLong;
	double minLat;
	double maxLat;
	int[] xPoints;
	int[] yPoints;
	boolean givenCoords = false;

	public MapPanel(ImageIcon imageIcon, int activityId) {

		this.imageIcon = imageIcon;
		image = imageIcon.getImage();
		JPanel panel = new JPanel();

		this.add(panel);
		PointsDAO dao = new PointsDAO();
		list = dao.getAll(activityId);
		if (list.size() > 0) {
			xPoints = new int[list.size()];
			yPoints = new int[list.size()];

			minLong = list.get(0).getLongitude();
			maxLong = list.get(0).getLongitude();
			minLat = list.get(0).getLatitude();
			maxLat = list.get(0).getLatitude();

			for (Point p : list) {
				if (p.getLongitude() > maxLong) {
					maxLong = p.getLongitude();
				} else if (p.getLongitude() < minLong) {
					minLong = p.getLongitude();
				}
				if (p.getLatitude() > maxLat) {
					maxLat = p.getLatitude();
				} else if (p.getLatitude() < minLat) {
					minLat = p.getLatitude();
				}
			}

			longDiff = maxLong - minLong;
			latDiff = maxLat - minLat;

			for (int i = 0; i < list.size(); i++) {
				yPoints[i] = (int) (((maxLat - list.get(i).getLatitude()) / latDiff)
						* image.getHeight(imageIcon.getImageObserver()));
				xPoints[i] = image.getWidth(imageIcon.getImageObserver())
						- (int) (((maxLong - list.get(i).getLongitude()) / longDiff)
								* image.getWidth(imageIcon.getImageObserver()));

			}
		}

	}

	public MapPanel(ImageIcon imageIcon, int activityId, double maxLong, double minLong, double maxLat, double minLat) {
		this(imageIcon, activityId);
		this.maxLong = maxLong;
		this.minLong = minLong;
		this.maxLat = maxLat;
		this.minLat = minLat;
		longDiff = maxLong - minLong;
		latDiff = maxLat - minLat;
		for (int i = 0; i < list.size(); i++) {
			yPoints[i] = (int) (((maxLat - list.get(i).getLatitude()) / latDiff)
					* image.getHeight(imageIcon.getImageObserver()));

			xPoints[i] = image.getWidth(imageIcon.getImageObserver())
					- (int) (((maxLong - list.get(i).getLongitude()) / longDiff)
							* image.getWidth(imageIcon.getImageObserver()));

		}

	}

	@Override
	public void paintComponent(Graphics g) {

		g.drawImage(image, 0, 0, this);
		// g.drawPolygon(polygon);
		if (list.size() > 0) {
			g.drawPolyline(xPoints, yPoints, list.size());
			g.setColor(Color.green);
			g.drawRect(xPoints[0], yPoints[0], 4, 4);
			g.setColor(Color.red);
			g.drawRect(xPoints[list.size() - 1], yPoints[list.size() - 1], 4, 4);
			System.out.println();
		}
	

	}

}
