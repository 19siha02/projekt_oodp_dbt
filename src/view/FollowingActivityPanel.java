package view;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import controller.Controller;

public class FollowingActivityPanel extends JPanel{
	JList<String> activityList;
	Vector<String> lista = new Vector<>();
	int signedInUser;
	int followingUser;
	JLabel test = new JLabel("sasd");
	Controller controller = Controller.getInstance();
	JPanel gridCenter = new JPanel(new GridLayout(1,1));
	public FollowingActivityPanel(int followingUser) {
		this.signedInUser=controller.getSignedInUserId();
		this.followingUser=followingUser;
		fillList();
		activityList= new JList<String>(lista);
		
		this.setLayout(new BorderLayout());
		
		
		activityList.setVisible(true);
		JScrollPane scroll = new JScrollPane();
		scroll.setViewportView(activityList);

		JButton generateMap = new JButton("Map");
		JPanel buttonPanel = new JPanel(new GridLayout(1, 2));
		buttonPanel.add(generateMap);
		
		
		generateMap.addActionListener(e->{
			JFileChooser fileChooser = new JFileChooser(System.getProperty("user.dir"));
			FileNameExtensionFilter filter = new FileNameExtensionFilter(
			        "PNG","PNG");
			fileChooser.setFileFilter(filter);
			int choice = fileChooser.showOpenDialog(this);
			if(choice!= JFileChooser.APPROVE_OPTION) return;
			ImageIcon imageIcon = new ImageIcon(fileChooser.getSelectedFile().getAbsolutePath());
			//Hårdkodad för Gävlekartan
			MapPanel mapPanel = new MapPanel(imageIcon,
					Integer.parseInt(activityList.getSelectedValue().substring(0, 4).trim()), 17.228101, 17.099927,
					60.666204, 60.620533);
			JFrame mapFrame = new JFrame();
			Image image = imageIcon.getImage();
			mapFrame.setSize(image.getWidth(imageIcon.getImageObserver()), image.getHeight(imageIcon.getImageObserver()));
			mapFrame.add(mapPanel);
			mapFrame.setVisible(true);
			
			
		});
		
		
		this.add(scroll,BorderLayout.WEST);
	
		this.add(buttonPanel, BorderLayout.NORTH);
		
		
		this.add(gridCenter, BorderLayout.CENTER);
		
		activityList.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
	
				if(evt.getClickCount()==2) {
					onMouseClick();
				}
				else if(evt.getClickCount()==3) {
					System.out.println(activityList.getSelectedValue());
				}
			}
			
		});
		activityList.updateUI();
	}
	public void onMouseClick() {
		gridCenter.removeAll();
		gridCenter.add(new ActivityView(Integer.parseInt(activityList.getSelectedValue().substring(0, 4).trim()),followingUser),BorderLayout.CENTER);
		
		this.revalidate();
		this.repaint();
		
		
	}
	
	public void fillList() {
		List<String> list = controller.getUsersActivities(followingUser);
		
		lista.clear();
		for (int i = 0; i < list.size(); i++) {
			lista.add(list.get(i));
		}
		

	}
}
