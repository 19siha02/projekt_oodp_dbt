package view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.*;

import controller.Controller;

public class MainFrame extends JFrame {

	private JTabbedPane tabbedPane = new JTabbedPane();
	List<Integer> followingIds;
	Controller controller = Controller.getInstance();

	public MainFrame() {
		showLogin();
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setSize(2200, 800);
		this.setVisible(true);
		this.add(tabbedPane);

	}

	public void createPanels() {
		tabbedPane.add("Profil", createUserPanel());
		tabbedPane.add("Aktiviteter", new ActivityPanel());
		tabbedPane.add("Följare", createFollowingPanel());
		if (controller.getSignedInUserId() == 5) {
			tabbedPane.add("Admin", createAdminPanel());
		}
	}

	public JPanel createAdminPanel() {
		JPanel mainPanel = new JPanel();
		JTabbedPane adminPane = new JTabbedPane();

		JPanel createPanel = new JPanel(new GridLayout(5, 2));
		JPanel deletePanel = new JPanel();
		JButton deleteUserButton = new JButton("Delete");
		JButton createUserButton = new JButton("Create");
		JButton updateUserButton = new JButton("Update");
		JTextField fNameField = new JTextField("First Name");
		JTextField lNameField = new JTextField("Last Name");
		JTextField passWordField = new JTextField("Password");
		JTextField emailField = new JTextField("email");
		JTextField ageField = new JTextField("age");
		JTextField weightField = new JTextField("weight");
		JTextField heightField = new JTextField("height");
		JTextField hrField = new JTextField("Max HR");

		createUserButton.addActionListener(e -> {

			JOptionPane.showMessageDialog(this,
					controller.saveUser(fNameField.getText(), lNameField.getText(), passWordField.getText(),
							emailField.getText(), Integer.parseInt(ageField.getText()),
							Double.parseDouble(weightField.getText()), Integer.parseInt(heightField.getText()),
							Integer.parseInt(hrField.getText())));
		});
		updateUserButton.addActionListener(e -> {

			JOptionPane.showMessageDialog(this,
					controller.updateUser(fNameField.getText(), lNameField.getText(), passWordField.getText(),
							emailField.getText(), Integer.parseInt(ageField.getText()),
							Double.parseDouble(weightField.getText()), Integer.parseInt(heightField.getText()),
							Integer.parseInt(hrField.getText())));
		});
		deleteUserButton.addActionListener(e -> {
			controller.deleteUser("email");
		});
		createPanel.add(fNameField);
		createPanel.add(lNameField);
		createPanel.add(passWordField);
		createPanel.add(emailField);
		createPanel.add(ageField);
		createPanel.add(weightField);
		createPanel.add(heightField);
		createPanel.add(hrField);
		createPanel.add(createUserButton);
		createPanel.add(updateUserButton);
		adminPane.setPreferredSize(new Dimension(600, 400));
		deletePanel.add(deleteUserButton);
		adminPane.add("Create", createPanel);
		adminPane.add("Delete", deletePanel);
		adminPane.add("Update", new JPanel());
		mainPanel.add(adminPane);
		return mainPanel;
	}

	public JPanel createUserPanel() {
		JPanel panel = new JPanel(new BorderLayout());
		String[] userData = controller.getUserInfo(controller.getSignedInUserId());
		String[] labels = { "First Name", "Last Name", "Email", "Age", "Weight", "Height", "Max Heartrate" };
		JTable userTable = new JTable(7, 2);
		for (int i = 0; i < labels.length; i++) {
			userTable.setValueAt(labels[i], i, 0);
			userTable.setValueAt(userData[i], i, 1);
		}

		panel.add(userTable, BorderLayout.CENTER);

		return panel;

	}

	public JPanel createFollowingPanel() {
		JPanel panel = new JPanel(new BorderLayout());
		JPanel allUsersPanel = new JPanel(new BorderLayout());
		JPanel followingListPanel = new JPanel(new BorderLayout());
		JButton stopFollowing = new JButton("Stop following");
		DefaultListModel<String> listModel = controller.getAllFollowingUsernames(controller.getSignedInUserId());
		JList<String> followingList = new JList<String>(listModel);
		followingListPanel.add(stopFollowing, BorderLayout.SOUTH);
		followingListPanel.add(followingList, BorderLayout.CENTER);
		followingListPanel.add(new JLabel("Following"), BorderLayout.NORTH);
		followingList.setToolTipText("Following");

		followingIds = controller.getAllFollowingIds(controller.getSignedInUserId());
		JList<String> allUsers = new JList<String>(controller.getAllUsers());
		JButton followUserButton = new JButton("Follow");
		allUsersPanel.add(new JLabel("All users"), BorderLayout.NORTH);
		allUsersPanel.add(allUsers, BorderLayout.CENTER);
		allUsersPanel.add(followUserButton, BorderLayout.SOUTH);

		followUserButton.addActionListener(e -> {
			if (controller.followUser(allUsers.getSelectedValue())) {

				listModel.addElement(allUsers.getSelectedValue());
				followingIds = controller.getAllFollowingIds(controller.getSignedInUserId());
				followingListPanel.revalidate();
				followingListPanel.repaint();
			}
		});
		stopFollowing.addActionListener(e -> {
			controller.stopFollowUser(followingList.getSelectedValue());
			followingIds = controller.getAllFollowingIds(controller.getSignedInUserId());
			listModel.remove(followingList.getSelectedIndex());
			followingListPanel.revalidate();
			followingListPanel.repaint();
		});
		followingList.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {

				if (evt.getClickCount() == 2) {
					BorderLayout layout = (BorderLayout) panel.getLayout();
					panel.remove(layout.getLayoutComponent(BorderLayout.CENTER));
					panel.add(new FollowingActivityPanel(followingIds.get(followingList.getSelectedIndex())),
							BorderLayout.CENTER);
					panel.revalidate();
					panel.repaint();
				} else if (evt.getClickCount() == 3) {

				}
			}

		});
		panel.add(new JPanel(), BorderLayout.CENTER);
		panel.add(allUsersPanel, BorderLayout.EAST);
		panel.add(followingListPanel, BorderLayout.WEST);
		// panel.add(allUsers);

		return panel;

	}

	public void showLogin() {
		JPanel loginPane = new JPanel(new GridLayout(6, 1));
		JButton loginButton = new JButton("Login");

		JButton cancelButton = new JButton("Exit");
		JLabel userLabel = new JLabel("Username(Email)");
		JLabel passwordLabel = new JLabel("Password");
		JTextField userField = new JTextField("");
		JPasswordField passField = new JPasswordField("");
		loginPane.add(userLabel);
		loginPane.add(userField);
		loginPane.add(passwordLabel);
		loginPane.add(passField);
		loginPane.add(loginButton);
		loginPane.add(cancelButton);
		JDialog loginDialog = new JDialog(this);
		loginDialog.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		loginDialog.setAlwaysOnTop(true);
		loginDialog.add(loginPane);
		loginDialog.setVisible(true);
		loginDialog.pack();
		loginPane.setVisible(true);
		loginButton.addActionListener(e -> {
			if (controller.login(userField.getText(), passField.getText()) != 0) {
				System.out.println("succesful");
				loginDialog.setVisible(false);
				this.setVisible(true);
				createPanels();

			} else {
				System.out.println("fail");

			}
		});
		cancelButton.addActionListener(e -> System.exit(0));
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> new MainFrame());
	}
}
