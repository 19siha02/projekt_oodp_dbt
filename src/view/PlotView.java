package view;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Iterator;
import java.util.List;

import javax.swing.*;

import controller.Controller;

public class PlotView extends JPanel {
	private static final long serialVersionUID = 1L;
	private List<Double> trackpoints;
	private List<Integer> timeList;
	private int width;
	private int height;
	private int totalElapsedTime; // related to width
	private double minDataValue; // related to height
	private double maxDataValue; // related to height
	private int[] xPixels;
	private int[] yPixels;

	public PlotView(String title, List<Double> trackpoints, List<Integer> timeList) {
		Controller.getInstance();
		this.trackpoints = trackpoints;
		this.timeList = timeList;
		setBackground(Color.WHITE);
		setBorder(BorderFactory.createTitledBorder(title));
	}


	private void findLimitsInData() {
		if (trackpoints != null && trackpoints.size() > 1) {

			minDataValue = maxDataValue = trackpoints.get(0);
			totalElapsedTime = timeList.get(timeList.size() - 1);
			for (double tp : trackpoints) {

				if (tp > maxDataValue)
					maxDataValue = tp;
				else if (tp < minDataValue)
					minDataValue = tp;
			}
		}
	}


	private void createArrays() {
		if (trackpoints != null && trackpoints.size() > 0) {
			findLimitsInData();

			width = getWidth();
			height = getHeight();
			yPixels = new int[width];
			xPixels = new int[width];
			double timeStep = (double) totalElapsedTime / width;
			double yVariation = maxDataValue - minDataValue;
			double yScale = height / yVariation;
			Iterator<Double> tpit = trackpoints.iterator();
			Iterator<Integer> tlit = timeList.iterator();
			
			if (tpit.hasNext()) {
				double tp = tpit.next();
				int tip = tlit.next();
				for (int x = 0; x < width; x++) {
					double time = x * timeStep;
					while (tpit.hasNext() && tip < time) {
						tp = tpit.next();
						tip = tlit.next();
						tp = tp - minDataValue;
					}
						
					
					
					yPixels[x] = (int) (height - (yScale * tp));
					xPixels[x] = x;
				}
			}
		}
	}

	


	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		createArrays();
		g.setColor(Color.BLUE);
		g.drawPolyline(xPixels, yPixels, width);
	}
}