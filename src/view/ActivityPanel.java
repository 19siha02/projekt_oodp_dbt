package view;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

import controller.Controller;

public class ActivityPanel extends JPanel implements FocusListener {
	JList<String> activityList;
	DefaultListModel<String> lista = new DefaultListModel<String>();
	int signedInUser;
	JLabel test = new JLabel("sasd");
	Controller controller = Controller.getInstance();
	JPanel gridCenter = new JPanel(new GridLayout(1, 1));
	JScrollPane scroll;

	public ActivityPanel() {
		this.signedInUser = controller.getSignedInUserId();

		activityList = new JList<String>(lista);
		fillList();
		this.setLayout(new BorderLayout());

		activityList.setVisible(true); 
		scroll = new JScrollPane();
		scroll.setViewportView(activityList);
		JButton importActivity = new JButton("Import Activity");
		JButton editActivity = new JButton("Edit Activity");
		JButton generateMap = new JButton("Map");
		generateMap.addActionListener(e -> {
			if(activityList.getSelectedValue()!=null) {
			JFileChooser fileChooser = new JFileChooser(System.getProperty("user.dir"));
			FileNameExtensionFilter filter = new FileNameExtensionFilter("PNG", "PNG");
			fileChooser.setFileFilter(filter);
			int choice = fileChooser.showOpenDialog(this);
			if (choice != JFileChooser.APPROVE_OPTION)
				return;
			ImageIcon imageIcon = new ImageIcon(fileChooser.getSelectedFile().getAbsolutePath());
			
			//MapPanel mapPanel = new MapPanel(imageIcon,
			//Integer.parseInt(activityList.getSelectedValue().substring(0, 4).trim()));
			//MapPanel mapPanel = new MapPanel(imageIcon,
			//		Integer.parseInt(activityList.getSelectedValue().substring(0, 4).trim()), 17.228101, 17.099927,
			//		60.666204, 60.620533);
			// Hårdkodad för lidingökartan
			MapPanel mapPanel = new MapPanel(imageIcon,
			Integer.parseInt(activityList.getSelectedValue().substring(0,
			4).trim()),18.243706,18.093855,59.390379,59.349271);

			JFrame mapFrame = new JFrame();
			Image image = imageIcon.getImage();
			mapFrame.setSize(image.getWidth(imageIcon.getImageObserver()),
					image.getHeight(imageIcon.getImageObserver()) + 50);
			mapFrame.add(mapPanel);
			mapFrame.setVisible(true);

		}
			else 
				JOptionPane.showMessageDialog(this, "Välj en aktivitet i listan att rita ut");});
		editActivity.addActionListener(e -> {
			if(activityList.getSelectedValue()!=null) {
			JPanel createActivityPanel = new JPanel();
			JTextField nameTextField = new JTextField(activityList.getSelectedValue().substring(7));
			JTextField locationTextField = new JTextField("Location");
			List<String> nameList = controller.getActivityCategoriesNames();
			List<Integer> idList = controller.getActivityCategoriesIds();
			String[] comboOptions = new String[nameList.size()];
			for (int i = 0; i < nameList.size(); i++) {
				comboOptions[i] = nameList.get(i);
			}

			JComboBox<String> categories = new JComboBox<>(comboOptions);
			createActivityPanel.add(nameTextField);
			createActivityPanel.add(locationTextField);
			createActivityPanel.add(categories);
			int result = JOptionPane.showConfirmDialog(null, createActivityPanel, "Edit Activity",
					JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
			if (result == JOptionPane.OK_OPTION) {
				controller.updateActivity(Integer.parseInt(activityList.getSelectedValue().substring(0, 4).trim()),
						nameTextField.getText(), locationTextField.getText(), signedInUser,
						idList.get(categories.getSelectedIndex()));
				fillList();
				activityList.updateUI();
			}
		}
			else 
				JOptionPane.showMessageDialog(this, "Välj en aktivitet i listan först");});
		importActivity.addActionListener(e -> {
			JFrame temp = new ImportFrame();
			temp.setVisible(true);

		});
		this.add(importActivity, BorderLayout.SOUTH);
		this.add(scroll, BorderLayout.WEST);
		this.add(editActivity, BorderLayout.EAST);
		this.add(generateMap, BorderLayout.NORTH);

		this.add(gridCenter, BorderLayout.CENTER);

		activityList.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {

				if (evt.getClickCount() == 2) {
					onMouseClick();
				} else if (evt.getClickCount() == 3) {
					System.out.println(activityList.getSelectedValue());
				}
			}

		});
		activityList.updateUI();
		activityList.addFocusListener(this);

	}

	public void onMouseClick() {
		gridCenter.removeAll();
		gridCenter.add(new ActivityView(Integer.parseInt(activityList.getSelectedValue().substring(0, 4).trim()),controller.getSignedInUserId()),
				BorderLayout.CENTER);

		this.revalidate();
		this.repaint();

	}

	public void fillList() {
		List<String> list = controller.getUsersActivities(signedInUser);

		lista.clear();
		for (int i = 0; i < list.size(); i++) {
			lista.addElement(list.get(i));
		}

	}

	@Override
	public void focusGained(FocusEvent e) {

		fillList();
		this.revalidate();
		this.repaint();

	}

	@Override
	public void focusLost(FocusEvent e) {
		// TODO Auto-generated method stub
		System.out.println("tappar fookus");
	}

}
