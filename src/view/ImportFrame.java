package view;

import java.awt.Dimension;
import java.util.List;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

import controller.Controller;

public class ImportFrame extends JFrame {
	JList<String> activityList;
	JPanel mainPanel;
	int signedInUser;
	String t;
	JTextArea text;
	Controller controller = Controller.getInstance();
	DefaultListModel<String> lista = new DefaultListModel<String>();

	public ImportFrame() {

		this.signedInUser = controller.getSignedInUserId();
		this.setSize(200, 200);
		this.setPreferredSize(new Dimension(200, 200));
		fillList();
		activityList = new JList<String>(lista);
		mainPanel = new JPanel();

		JButton chooseFile = new JButton("Choose file");

		JFileChooser fileChooser = new JFileChooser(System.getProperty("user.dir"));
		FileNameExtensionFilter filter = new FileNameExtensionFilter("CSV", "CSV");
		fileChooser.setFileFilter(filter);
		text = new JTextArea();
		chooseFile.addActionListener(e -> {
			;
			int choice = fileChooser.showOpenDialog(this);
			if (choice != JFileChooser.APPROVE_OPTION)
				return;
			t = fileChooser.getSelectedFile().getAbsolutePath();
			text.setText(t);
			this.setTitle(t);

		});
		text = new JTextArea();
		text.setEditable(false);
		mainPanel.add(text);
		activityList.setVisible(true);
		JButton createActivity = new JButton("Create Activity");
		JButton importToActivity = new JButton("Import");
		JScrollPane scroll = new JScrollPane();
		scroll.setViewportView(activityList);
		mainPanel.add(scroll);
		mainPanel.add(chooseFile);
		mainPanel.add(importToActivity);
		mainPanel.add(createActivity);
		importToActivity.addActionListener(e -> {
			controller.importCsv(text.getText(),
					Integer.parseInt(activityList.getSelectedValue().substring(0, 4).trim()));
			this.setVisible(false);

		});
		createActivity.addActionListener(e -> {
			createActivity();
		});
		this.add(mainPanel);

		this.setVisible(true);

	}

	public void fillList() {
		List<String> list = controller.getUsersActivities(signedInUser);
	

		lista.clear();
		for (int i = 0; i < list.size(); i++) {
			lista.addElement(list.get(i));
		}
		
	}

	public void createActivity() {
		JPanel createActivityPanel = new JPanel();
		JTextField nameTextField = new JTextField("Name");
		JTextField locationTextField = new JTextField("Location");
		List<String> nameList = controller.getActivityCategoriesNames();
		List<Integer> idList = controller.getActivityCategoriesIds();
		String[] comboOptions = new String[nameList.size()];
		for (int i = 0; i < nameList.size(); i++) {
			comboOptions[i] = nameList.get(i);
		}

		JComboBox<String> categories = new JComboBox<>(comboOptions);
		createActivityPanel.add(nameTextField);
		createActivityPanel.add(locationTextField);
		createActivityPanel.add(categories);
		int result = JOptionPane.showConfirmDialog(null, createActivityPanel, "Create activity",
				JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
		if (result == JOptionPane.OK_OPTION) {
			controller.createActivity(nameTextField.getText(), locationTextField.getText(), signedInUser,
					idList.get(categories.getSelectedIndex()));
			fillList();
			createActivityPanel.revalidate();
			createActivityPanel.repaint();
		}
	}

}
