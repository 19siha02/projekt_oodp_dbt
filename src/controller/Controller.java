package controller;

import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultListModel;

import dao.*;

import model.*;

public class Controller {

	private UserDAO userDao = new UserDAO();
	private ActivityDAO actDao = new ActivityDAO();
	private PointsDAO pointsDao = new PointsDAO();
	private CategoryDAO catDao = new CategoryDAO();
	private FollowingDAO follDao = new FollowingDAO();
	private static Controller instance;
	private User signedInUser;
	private List<User> allUserList;
	private List<ActivityCategory> allCategoriesList;

	private Controller() {
		allUserList = userDao.getAll();
		allCategoriesList = catDao.getAll();
	}

	public static Controller getInstance() {
		if (instance == null) {
			instance = new Controller();
		}
		return instance;
	}

	public int getSignedInUserId() {
		return signedInUser.getUserId();
	}

	public int login(String email, String password) {
		User user = userDao.get(email);
		if (user.getPassword().equals(password)) {

			System.out.println(user.getPassword() + " - " + password);
			signedInUser = user;
			setActivityList(user);
			setFollowingList(user);
			return user.getUserId();
		} else {
			System.out.println(user.getPassword() + " - " + password);
			return 0;
		}
	}

	public List<String> getUsersActivities(int userId) {
		List<Activity> list = null;
		for (User u : allUserList) {
			if (u.getUserId() == userId) {
				if (u.getActivityList() == null || u.getUserId() == signedInUser.getUserId()) {
					u.setActivityList(actDao.getAll(userId));
					list = u.getActivityList();
				} else
					list = u.getActivityList();
			}
		}

		List<String> returnList = new ArrayList<String>();
		for (Activity a : list) {
			returnList.add(String.format("%4d - %s", a.getActivityId(), a.getName()));
		}
		return returnList;
	}

	public void importCsv(String filePath, int activityId) {
		CSVReader reader = new CSVReader(filePath, activityId);
		pointsDao.saveList(reader.getList());
		Activity act = new Activity("", "", 0, 0);
		act.setList(reader.getList());
		if (Statistics.maxHeartRate(act) > signedInUser.getMaxHeartRate()) {
			signedInUser.setMaxHeartRate((int) Statistics.maxHeartRate(act));
			userDao.update(signedInUser);
		}
	}

	public String[] getStats(int activityId, int userId) {
		Activity activity = null;
		User user = null;
		for (User u : allUserList) {
			if (u.getUserId() == userId) {
				user = u;
			}
		}
		for (Activity a : user.getActivityList()) {
			if (a.getActivityId() == activityId) {
				activity = a;
			}
		}
		if (activity.getList() == null) {
			activity.setList(pointsDao.getAll(activity.getActivityId()));
		}

		if (activity.getList().size() != 0) {
			String[] returnArray = new String[19];
			returnArray[0] = Statistics.startTime(activity);
			returnArray[1] = Statistics.endTime(activity);
			returnArray[2] = String.format("%dh %dm %ds", Statistics.elapsedTime(activity) / 3600,
					(Statistics.elapsedTime(activity) % 3600) / 60, Statistics.elapsedTime(activity) % 60);
			returnArray[3] = String.format("%.1f m", Statistics.totalDistance(activity));
			returnArray[4] = String.format("%.2f BPM", Statistics.minHeartRate(activity));
			returnArray[5] = String.format("%.2f BPM", Statistics.maxHeartRate(activity));
			returnArray[6] = String.format("%.2f BPM", Statistics.avgHeartRate(activity));
			returnArray[7] = String.format("%.2f km/h", Statistics.minSpeed(activity));
			returnArray[8] = String.format("%.2f km/h", Statistics.maxSpeed(activity));
			returnArray[9] = String.format("%.2f km/h", Statistics.avgSpeed(activity));
			returnArray[10] = String.format("%.1f m", Statistics.minAltitude(activity));
			returnArray[11] = String.format("%.1f m", Statistics.maxAltitude(activity));
			returnArray[12] = String.format("%.1f m", Statistics.avgAltitude(activity));
			returnArray[13] = String.format("%.2f \u00B0", Statistics.minIncline(activity));
			returnArray[14] = String.format("%.2f \u00B0", Statistics.maxIncline(activity));
			returnArray[15] = String.format("%.2f \u00B0", Statistics.avgIncline(activity));
			returnArray[16] = String.format("%.1f st/min", Statistics.minCadence(activity));
			returnArray[17] = String.format("%.1f st/min", Statistics.maxCadence(activity));
			returnArray[18] = String.format("%.1f st/min", Statistics.avgCadence(activity));

			return returnArray;
		} else
			return null;
	}

	public void createActivity(String name, String location, int userId, int categoryId) {
		Activity activity = new Activity(name, location, userId, categoryId);
		activity = actDao.save(activity);
		if (activity.getActivityId() != 0) {
			signedInUser.getActivityList().add(activity);
		}

	}

	public String[] getUserInfo(int userId) {
		User user = null;
		for (User u : allUserList) {
			if (u.getUserId() == userId) {
				user = u;
			}
		}
		String[] returnValues = new String[7];
		returnValues[0] = user.getFirstName();
		returnValues[1] = user.getLastName();
		returnValues[2] = user.getEmail();
		returnValues[3] = Integer.toString(user.getAge());
		returnValues[4] = Double.toString(user.getWeight()) + " kg";
		returnValues[5] = Integer.toString(user.getHeight()) + " cm";
		returnValues[6] = Integer.toString(user.getMaxHeartRate()) + " BPM";
		return returnValues;
	}

	public List<Double> getActivityGraphData(int userId, int activityId, int dataType) {

		Activity activity = null;
		User user = null;
		for (User u : allUserList) {
			if (u.getUserId() == userId) {
				user = u;
			}
		}
		for (Activity a : user.getActivityList()) {
			if (a.getActivityId() == activityId) {
				activity = a;
			}
		}

		if (activity.getList() == null) {
			activity.setList(pointsDao.getAll(activity.getActivityId()));
		}

		List<Double> dataPoints = new ArrayList<Double>();

		switch (dataType) {
		case 0:
			for (Point p : activity.getList()) {
				dataPoints.add(p.getHeartRate());

			}
			break;
		case 1:
			for (Point p : activity.getList()) {
				dataPoints.add(p.getSpeed());

			}
			break;
		case 2:
			for (Point p : activity.getList()) {
				dataPoints.add(p.getAltitude());

			}
			break;
		case 3:
			for (Point p : activity.getList()) {
				dataPoints.add(p.getCadence());

			}
			break;
		}

		return dataPoints;

	}

	public List<Integer> getActivityGraphTime(int userId, int activityId) {
		Activity activity = null;
		User user = null;
		for (User u : allUserList) {
			if (u.getUserId() == userId) {
				user = u;
			}
		}
		for (Activity a : user.getActivityList()) {
			if (a.getActivityId() == activityId) {
				activity = a;
			}
		}

		if (activity.getList() == null) {
			activity.setList(pointsDao.getAll(activity.getActivityId()));
		}

		List<Integer> timePoints = new ArrayList<Integer>();

		for (Point p : activity.getList()) {

			timePoints.add(p.getElapsedTime());
		}

		return timePoints;

	}

	public List<String> getActivityCategoriesNames() {

		List<String> returnList = new ArrayList<String>();
		for (ActivityCategory c : allCategoriesList) {
			returnList.add(c.getName());
		}

		return returnList;
	}

	public List<Integer> getActivityCategoriesIds() {

		List<Integer> returnList = new ArrayList<Integer>();
		for (ActivityCategory c : allCategoriesList) {
			returnList.add(c.getId());
		}
		return returnList;
	}

	public void updateActivity(int activityId, String name, String location, int userId, int categoryId) {
		Activity activity = new Activity(activityId, name, location, userId, categoryId);
		actDao.update(activity);
	}

	public DefaultListModel<String> getAllFollowingUsernames(int signedInUser) {
		DefaultListModel<String> returnList = new DefaultListModel<String>();
		List<User> userList = this.signedInUser.getFollowingList();
		for (User u : userList)
			returnList.addElement(u.getEmail());

		return returnList;
	}

	public List<Integer> getAllFollowingIds(int signedInUser) {
		List<Integer> returnList = new ArrayList<Integer>();
		List<User> userList = this.signedInUser.getFollowingList();
		for (User u : userList)
			returnList.add(u.getUserId());

		return returnList;
	}

	public List<Integer> getAllFollowerIds(int signedInUser) {
		List<Integer> returnList = new ArrayList<Integer>();
		List<User> userList = follDao.getAllFollowers(signedInUser);
		for (User u : userList)
			returnList.add(u.getUserId());

		return returnList;
	}

	public double calcCalories(int activityId, int userId) {
		Activity activity = null;
		User user = null;
		for (User u : allUserList) {
			if (userId == u.getUserId())
				user = u;
		}

		for (Activity a : user.getActivityList()) {
			if (a.getActivityId() == activityId) {
				activity = a;
			}
		}

		if (activity.getList() == null) {
			activity.setList(pointsDao.getAll(activity.getActivityId()));
		}

		ActivityCategory category = null;
		for (ActivityCategory c : allCategoriesList) {
			if (c.getId() == activity.getCategoryId())
				category = c;
		}

		double calories = ((double) Statistics.elapsedTime(activity) / 3600) * user.getWeight()
				* category.getDifficulty();

		return calories;
	}

	public String saveUser(String firstName, String lastName, String passWord, String email, int age, double weight,
			int height, int maxHeartRate) {
		User createdUser = new User(0, firstName, lastName, passWord, email, age, weight, height, maxHeartRate);
		User savedUser = userDao.save(createdUser);
		return String.format("Saved user %s %s with user id:%d", savedUser.getFirstName(), savedUser.getLastName(),
				savedUser.getUserId());

	}

	public String updateUser(String firstName, String lastName, String passWord, String email, int age, double weight,
			int height, int maxHeartRate) {
		User createdUser = new User(0, firstName, lastName, passWord, email, age, weight, height, maxHeartRate);
		User savedUser = userDao.update(createdUser);
		return String.format("Updated user %s %s with user id:%d", savedUser.getFirstName(), savedUser.getLastName(),
				savedUser.getUserId());

	}

	public void deleteUser(String username) {
		User deleteUser = new User(0, "", "", "", username, 0, 0, 0, 0);
		userDao.delete(deleteUser);
	}

	public DefaultListModel<String> getAllUsers() {
		List<User> list = userDao.getAll();
		DefaultListModel<String> emails = new DefaultListModel<String>();
		for (User u : list) {
			emails.addElement(u.getEmail());
		}
		return emails;
	}

	public boolean followUser(String email) {

		User user = null;
		for (User u : allUserList) {
			if (email.equals(u.getEmail()))
				user = u;
		}

		if (follDao.save(signedInUser, user)) {
			signedInUser.getFollowingList().add(user);
			return true;
		}

		else
			return false;

	}

	public void stopFollowUser(String email) {
		User user = null;
		for (User u : allUserList) {
			if (email.equals(u.getEmail()))
				user = u;
		}
		signedInUser.getFollowingList().remove(user);
		follDao.delete(signedInUser, user);
	}

	private void setActivityList(User user) {
		user.setActivityList(actDao.getAll(user.getUserId()));

	}

	private void setFollowingList(User user) {
		user.setFollowingList(follDao.getAllFollowing(user.getUserId()));
	}
}
