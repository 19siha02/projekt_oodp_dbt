package model;

import java.util.List;

public class User {
	private int userId;
	private String firstName;
	private String lastName;
	private String email;
	private int age;
	private String password;
	private double weight;
	private int height;
	private int maxHeartRate;
	private List<Activity> activityList;
	private List<User> followingList;
	
	public User(int userId, String firstName, String lastName,String password,String email, 
			int age, double weight, int height, int maxHeartRate) {
		this.userId=userId;
		this.firstName=firstName;
		this.lastName=lastName;
		this.password=password;
		this.email=email;
		this.age=age;
		this.weight=weight;
		this.height=height;
		this.maxHeartRate=maxHeartRate;
		
	}
	
	
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}

	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public double getWeight() {
		return weight;
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public int getMaxHeartRate() {
		return maxHeartRate;
	}
	public void setMaxHeartRate(int maxHeartRate) {
		this.maxHeartRate = maxHeartRate;
	}
	public List<Activity> getActivityList() {
		return activityList;
	}
	public void setActivityList(List<Activity> activityList) {
		this.activityList = activityList;
	}
	public List<User> getFollowingList() {
		return followingList;
	}
	public void setFollowingList(List<User> followingList) {
		this.followingList = followingList;
	}
	
	
	
}
