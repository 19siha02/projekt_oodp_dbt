package model;

public class Point {
	private int id;
	private String date;
	private String time;
	private int elapsedTime;
	private double latitude;
	private double longitude;
	private double altitude;
	private double distance;
	private double heartRate;
	private double speed;
	private double cadence;
	public int getActivityId() {
		return activityId;
	}

	private int activityId;
	
	public Point( String date, String time, int elapsedTime, double longitude,double latitude, double altitude, 
			double distance, double heartRate, double speed, double cadence, int activityId) {
		this.date = date;
		this.time = time;
		this.elapsedTime = elapsedTime;
		this.latitude = latitude;
		this.longitude = longitude;
		this.altitude = altitude;
		this.distance = distance;
		this.heartRate = heartRate;
		this.speed = speed;
		this.cadence = cadence;
		this.activityId=activityId;
	}
	public Point(int id, String date, String time, int elapsedTime,double longitude,double latitude,  double altitude, 
			double distance, double heartRate, double speed, double cadence) {
		this.id = id;
		this.date = date;
		this.time = time;
		this.elapsedTime = elapsedTime;
		this.latitude = latitude;
		this.longitude = longitude;
		this.altitude = altitude;
		this.distance = distance;
		this.heartRate = heartRate;
		this.speed = speed;
		this.cadence = cadence;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public int getElapsedTime() {
		return elapsedTime;
	}

	public void setElapsedTime(int elapsedTime) {
		this.elapsedTime = elapsedTime;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getAltitude() {
		return altitude;
	}

	public void setAltitude(double altitude) {
		this.altitude = altitude;
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	public double getHeartRate() {
		return heartRate;
	}

	public void setHeartRate(double heartRate) {
		this.heartRate = heartRate;
	}

	public double getSpeed() {
		return speed;
	}

	public void setSpeed(double speed) {
		this.speed = speed;
	}

	public double getCadence() {
		return cadence;
	}

	public void setCadence(double cadence) {
		this.cadence = cadence;
	}
	
}
