package model;

public class ActivityCategory {
	private String name;
	private int id;
	private int difficulty;
	
	public ActivityCategory(int id, String name, int difficulty) {
		this.id=id;
		this.name=name;
		this.difficulty=difficulty;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getDifficulty() {
		return difficulty;
	}
	public void setDifficulty(int difficulty) {
		this.difficulty = difficulty;
	}
	
}
