package model;

public class Statistics {

	public static double avgHeartRate(Activity activity) {
		double sum = 0;
		int size = activity.getList().size();
		for(Point p:activity.getList()) {
			
			sum+=p.getHeartRate();
			if(p.getHeartRate()==0)//förädling
				size--;
		}
		
		return sum/size;
	}
	public static double maxHeartRate(Activity activity) {
		double max=activity.getList().get(0).getHeartRate();
		for(Point p:activity.getList()) {
			if(p.getHeartRate()>max)
				max=p.getHeartRate();
			
		}
		return max;
	}
	public static double minHeartRate(Activity activity) {
		double min=activity.getList().get(0).getHeartRate();
		for(Point p:activity.getList()) {
			if(p.getHeartRate()<min&&p.getHeartRate()>0)//förädling
				min=p.getHeartRate();
			
		}
		return min;
	}
	public static double totalDistance(Activity activity) {
		double max=0;
		//activity.getList().get(activity.getList().size()-1).getDistance();
		for(Point p:activity.getList()) {
			if(p.getDistance()>max)
				max=p.getDistance();
			
		}
		return max;
	}
	public static String startTime(Activity activity) {
		return activity.getList().get(0).getTime();
		
	}
	public static String endTime(Activity activity) {
		return activity.getList().get(activity.getList().size()-1).getTime();	
	}
	public static int elapsedTime(Activity activity) {
		return activity.getList().get(activity.getList().size()-1).getElapsedTime();
		
	}
	public static double maxAltitude(Activity activity) {
		double max=activity.getList().get(0).getAltitude();
		for(Point p:activity.getList()) {
			if(p.getAltitude()>max)
				max=p.getAltitude();
			
		}
		return max;
	}
	public static double minAltitude(Activity activity) {
		double min=activity.getList().get(0).getAltitude();
		for(Point p:activity.getList()) {
			if(p.getAltitude()<min)
				min=p.getAltitude();
			
		}
		return min;
	}
	public static double avgAltitude(Activity activity) {
		double sum = 0;
		for(Point p:activity.getList()) {
			sum+=p.getAltitude();
		}
		
		return sum/activity.getList().size();
	}
	public static double maxSpeed(Activity activity) {
		double max=activity.getList().get(0).getSpeed();
		for(Point p:activity.getList()) {
			if(p.getSpeed()>max)
				max=p.getSpeed();
			
		}
		return max;
	}
	public static double minSpeed(Activity activity) {
		double min=activity.getList().get(0).getSpeed();
		for(Point p:activity.getList()) {
			if(p.getSpeed()<min&&p.getSpeed()>0)//förädling
				min=p.getSpeed();
			
		}
		return min;
	}
	public static double avgSpeed(Activity activity) {
		double sum = 0;
		int size = activity.getList().size();
		for(Point p:activity.getList()) {
			
			sum+=p.getSpeed();
			if(p.getSpeed()==0)//förädling
				size--;
		}
		
		return sum/size;
	}
	public static double maxCadence(Activity activity) {
		double max=activity.getList().get(0).getCadence();
		for(Point p:activity.getList()) {
			if(p.getCadence()>max)
				max=p.getCadence();
			
		}
		return max;
	}
	public static double minCadence(Activity activity) {
		double min=activity.getList().get(0).getCadence();
		for(Point p:activity.getList()) {
			if(p.getCadence()<min&&p.getCadence()>0)
				min=p.getCadence();
			
		}
		return min;
	}
	public static double avgCadence(Activity activity) {
		double sum = 0;
		int size = activity.getList().size();
		for(Point p:activity.getList()) {
			
			sum+=p.getCadence();
			if(p.getCadence()==0)//förädling
				size--;
		}
		
		return sum/size;
	}
	public static double maxIncline(Activity activity) {
		double heightDifference = Math.abs(activity.getList().get(0).getAltitude()-activity.getList().get(1).getAltitude());
		double lengthDifference = Math.abs(activity.getList().get(0).getDistance()-activity.getList().get(1).getDistance());
		double maxIncline= (Math.atan(heightDifference/lengthDifference)*(180/Math.PI));
		
		for(int i=1;i<activity.getList().size()-1;i++) {
			heightDifference = activity.getList().get(i).getAltitude()-activity.getList().get(i+1).getAltitude();
			lengthDifference = Math.abs(activity.getList().get(i).getDistance()-activity.getList().get(i+1).getDistance());
			if((Math.atan(heightDifference/lengthDifference)*(180/Math.PI))>maxIncline) {
				maxIncline=(Math.atan(heightDifference/lengthDifference)*(180/Math.PI));
				
			}
			
		}
		
		return maxIncline;
	}
	public static double minIncline(Activity activity) {
		double heightDifference = Math.abs(activity.getList().get(0).getAltitude()-activity.getList().get(1).getAltitude());
		double lengthDifference = Math.abs(activity.getList().get(0).getDistance()-activity.getList().get(1).getDistance());
		double minIncline= (Math.atan(heightDifference/lengthDifference)*(180/Math.PI));
		for(int i=1;i<activity.getList().size()-1;i++) {
			heightDifference = activity.getList().get(i).getAltitude()-activity.getList().get(i+1).getAltitude();
			lengthDifference = Math.abs(activity.getList().get(i).getDistance()-activity.getList().get(i+1).getDistance());
			if((Math.atan(heightDifference/lengthDifference)*(180/Math.PI))<minIncline) {
				minIncline=(Math.atan(heightDifference/lengthDifference)*(180/Math.PI));
			}
			
		}
		return minIncline;
		
	}
	public static double avgIncline(Activity activity) {
		double heightDifference;
		double lengthDifference;
		double totalIncline=0;
		for(int i=0;i<activity.getList().size()-1;i++) {
			heightDifference = activity.getList().get(i).getAltitude()-activity.getList().get(i+1).getAltitude();
			lengthDifference = activity.getList().get(i).getDistance()-activity.getList().get(i+1).getDistance();
			
			if(heightDifference!=0||lengthDifference!=0)
			totalIncline+=(Math.atan(heightDifference/lengthDifference)*(180/Math.PI));
			
			
		}
		
		
		return totalIncline/activity.getList().size();
	}
	/*public static String[] getAll() {
		
	}*/
	
	
}
