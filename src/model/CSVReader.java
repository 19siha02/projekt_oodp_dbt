package model;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;



public class CSVReader {
	
    
    
	private List<Point> list = new ArrayList<Point>();
    public CSVReader(String filePath, int activityId) {
    	
    	Scanner fileScanner = null;

		try {
			fileScanner = new Scanner(new FileReader(filePath));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		fileScanner.nextLine();//hyva bort rubrikerna
    	while (fileScanner.hasNextLine()) {
        	String temp[] = fileScanner.nextLine().split(";");
        	for(int i=0;i<temp.length;i++) {
        		
        		temp[i]=temp[i].replace(",", ".");
        		
        	}   	
        	list.add(new Point(temp[0], temp[1], Integer.parseInt(temp[2]),Float.parseFloat(temp[3]) , Float.parseFloat(temp[4]), Float.parseFloat(temp[5]), 
        			Float.parseFloat(temp[6]), Float.parseFloat(temp[7]), Float.parseFloat(temp[8]), Float.parseFloat(temp[9]), activityId));
        	System.out.println();
        	
        }
    	
        fileScanner.close();
	}
	public List<Point> getList() {
		return list;
	}
    
  	
    
      
    
   
}
