package model;

import java.util.List;

public class Activity {
	private List<Point> list=null;
	private String name;
	private String location;
	private int userId;
	private int activityId;
	private int categoryId;
	
	public int getActivityId() {
		return activityId;
	}

	public Activity(String name, String location, int userId, int categoryId) {
		this.name=name;
		this.location=location;
		this.userId=userId;
		this.categoryId=categoryId;
		
	}
	public Activity(int generatedId, String name, String location, int userId, int categoryId) {
		// TODO Auto-generated constructor stub
		this.activityId=generatedId;
		this.name=name;
		this.location=location;
		this.userId=userId;
		this.categoryId=categoryId;
		
	}
	
	public void  setList(List<Point> list) {
		 this.list = list;
	}
	
	public List<Point> getList() {
		return list;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getName() {
		return name;
	}
	
	public String getLocation() {
		return location;
	}
	
	public int getUserId() {
		return userId;
	}
	public int getCategoryId() {
		// TODO Auto-generated method stub
		return categoryId;
	}
	
}
