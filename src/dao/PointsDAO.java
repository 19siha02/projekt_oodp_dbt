package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import db.DbConnectionManager;
import model.Point;

public class PointsDAO implements IDao<Point> {
	DbConnectionManager dbConManagerSingleton = null;

	public PointsDAO() {
		dbConManagerSingleton = DbConnectionManager.getInstance();
	}

	@Override
	public Point get(int id) throws NoSuchElementException {
		Point point = null;
		try {
			ResultSet resultSet = dbConManagerSingleton.excecuteQuery(
					"SELECT date, time, elapsed_time, longitude, latitude,altitude, distance, heart_rate, speed, cadence, activity_id FROM points WHERE id="
							+ id);
			if (!resultSet.next())
				throw new NoSuchElementException("The point with id " + id + " doesen't exist in database");
			else
				point = new Point(resultSet.getString(1), resultSet.getString(2), resultSet.getInt(3),
						resultSet.getFloat(4), resultSet.getFloat(5), resultSet.getFloat(6), resultSet.getFloat(7),
						resultSet.getInt(8), resultSet.getFloat(9), resultSet.getFloat(10), resultSet.getInt(11));
			dbConManagerSingleton.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return point;
	}

	@Override
	public List<Point> getAll() {

		ArrayList<Point> list = new ArrayList<>();

		try {
			ResultSet resultSet = dbConManagerSingleton.excecuteQuery(
					"SELECT date, time, elapsed_time,longitude, latitude, altitude, distance, heart_rate, speed, cadence, activity_id FROM points");
			while (resultSet.next()) {
				list.add(new Point(resultSet.getString(1), resultSet.getString(2), resultSet.getInt(3),
						resultSet.getFloat(4), resultSet.getFloat(5), resultSet.getFloat(6), resultSet.getFloat(7),
						resultSet.getInt(8), resultSet.getFloat(9), resultSet.getFloat(10), resultSet.getInt(11)));

			}
			dbConManagerSingleton.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	public List<Point> getAll(int activityId) {

		ArrayList<Point> list = new ArrayList<>();

		try {
			ResultSet resultSet = dbConManagerSingleton.excecuteQuery(
					"SELECT date, time, elapsed_time,  longitude, latitude, altitude, distance, heart_rate, speed, cadence FROM points WHERE activity_id="
							+ activityId);
			while (resultSet.next()) {
				list.add(new Point(resultSet.getString(1), resultSet.getString(2), resultSet.getInt(3),
						resultSet.getFloat(4), resultSet.getFloat(5), resultSet.getFloat(6), resultSet.getFloat(7),
						resultSet.getInt(8), resultSet.getFloat(9), resultSet.getFloat(10), activityId));

			}
			dbConManagerSingleton.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public Point save(Point t) {
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
	
			preparedStatement = dbConManagerSingleton.prepareStatement(
					"INSERT INTO points (date, time, elapsed_time, latitude, longitude,altitude, distance, heart_rate, speed, cadence, activity_id) "
							+ "VALUES (?::date, ?::time without time zone, ?, ?, ?, ?, ?, ?, ?, ?,? ) RETURNING point_id;");
			preparedStatement.setString(1, t.getDate());
			preparedStatement.setString(2, t.getTime());
			preparedStatement.setInt(3, t.getElapsedTime());
			preparedStatement.setDouble(4, t.getLatitude());
			preparedStatement.setDouble(5, t.getLongitude());
			preparedStatement.setDouble(6, t.getAltitude());
			preparedStatement.setDouble(7, t.getDistance());
			preparedStatement.setDouble(8, t.getHeartRate());
			preparedStatement.setDouble(9, t.getSpeed());
			preparedStatement.setDouble(10, t.getCadence());
			preparedStatement.setInt(11, t.getActivityId());

			preparedStatement.execute();
			resultSet = preparedStatement.getResultSet();
			resultSet.next();
			int generatedId = resultSet.getInt(1);
			dbConManagerSingleton.close();
			return new Point(generatedId, t.getDate(), t.getTime(), t.getElapsedTime(), t.getLatitude(),
					t.getLongitude(), t.getAltitude(), t.getDistance(), t.getHeartRate(), t.getSpeed(), t.getCadence());

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return new Point(0, "No Name", null, 0, 0, 0, 0, 0, 0, 0, 0);
	}

	public void saveList(List<Point> list) {
		PreparedStatement preparedStatement = null;


		try {
		
			preparedStatement = dbConManagerSingleton.prepareStatement(
					"INSERT INTO points (date, time, elapsed_time, latitude, longitude,altitude, distance, heart_rate, speed, cadence, activity_id) "
							+ "VALUES (?::date, ?::time without time zone, ?, ?, ?, ?, ?, ?, ?, ?,? ) RETURNING point_id;");
			for (Point t : list) {
				preparedStatement.setString(1, t.getDate());
				preparedStatement.setString(2, t.getTime());
				preparedStatement.setInt(3, t.getElapsedTime());
				preparedStatement.setDouble(4, t.getLatitude());
				preparedStatement.setDouble(5, t.getLongitude());
				preparedStatement.setDouble(6, t.getAltitude());
				preparedStatement.setDouble(7, t.getDistance());
				preparedStatement.setDouble(8, t.getHeartRate());
				preparedStatement.setDouble(9, t.getSpeed());
				preparedStatement.setDouble(10, t.getCadence());
				preparedStatement.setInt(11, t.getActivityId());
				preparedStatement.execute();
			}

			dbConManagerSingleton.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		
	}


	@Override
	public void update(Point t, String[] params) {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(Point t) {
		// TODO Auto-generated method stub

	}
}
