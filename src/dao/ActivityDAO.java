package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import db.DbConnectionManager;
import model.Activity;

public class ActivityDAO implements IDao<Activity> {
	DbConnectionManager dbConManagerSingleton = null;
	PointsDAO pointDao;
	public ActivityDAO() {
		dbConManagerSingleton = DbConnectionManager.getInstance();
	}
	@Override
	public Activity get(int id) {
		Activity activity = null;
		try{
			ResultSet resultSet = dbConManagerSingleton.excecuteQuery("SELECT * FROM activities WHERE activity_id=" + id);
			if( !resultSet.next())
				throw new NoSuchElementException("The activity with id " + id + " doesen't exist in database");
			else
				activity = new Activity(id, resultSet.getString(2),resultSet.getString(3),resultSet.getInt(4), resultSet.getInt(5));
			dbConManagerSingleton.close();
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		
		return activity;
	}

	@Override
	public List<Activity> getAll() {
		ArrayList<Activity> list = new ArrayList<>();
	
		return list;
	}
	public List<Activity> getAll(int userId) {
		ArrayList<Activity> list = new ArrayList<>();
		
		try {
			ResultSet resultSet = dbConManagerSingleton.excecuteQuery("SELECT * FROM activities WHERE user_id="+userId+"Order by activity_id ASC");
			while (resultSet.next()) {
				list.add(new Activity(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3), resultSet.getInt(4), resultSet.getInt(5))
						);
				
			}
			dbConManagerSingleton.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		System.out.println("Hämtade aktiviteter för "+userId);
		return list;
	
	}

	@Override
	public Activity save(Activity t) {
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
	
		try {
		
			preparedStatement = dbConManagerSingleton.prepareStatement(
											  "INSERT INTO activities (name, location, user_id, category_id) " +
											  "VALUES (?, ?, ?,?) RETURNING activity_id;");
			preparedStatement.setString(1, t.getName());
			preparedStatement.setString(2, t.getLocation());
			preparedStatement.setInt(3, t.getUserId());
			preparedStatement.setInt(4, t.getCategoryId());

			
			preparedStatement.execute();
			resultSet = preparedStatement.getResultSet();
			resultSet.next();
			int generatedId = resultSet.getInt(1);
			return new Activity(generatedId, t.getName(), t.getLocation(), t.getUserId(), t.getCategoryId());

		}
		catch ( SQLException e) {
			e.printStackTrace();
		}
		
		return new Activity(0, "fail", null, 0,0);
	}

	
	public void update(Activity t) {
		// TODO Auto-generated method stub
		PreparedStatement preparedStatement = null;
		
	
		try {
	
			preparedStatement = dbConManagerSingleton.prepareStatement(
											  "UPDATE activities Set name=?, location=?, category_id=? Where activity_id=? ");
											 
			preparedStatement.setString(1, t.getName());
			preparedStatement.setString(2, t.getLocation());;
			preparedStatement.setInt(3, t.getCategoryId());
			preparedStatement.setInt(4, t.getActivityId());
			preparedStatement.execute();
			
			
			

		}
		catch ( SQLException e) {
			e.printStackTrace();
		}
		
		
	}

	@Override
	public void delete(Activity t) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void update(Activity t, String[] params) {
		// TODO Auto-generated method stub
		
	}

}
