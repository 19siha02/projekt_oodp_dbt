package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import db.DbConnectionManager;
import model.User;

public class FollowingDAO implements IDao<User> {
	
	DbConnectionManager dbConManagerSingleton = null;
	UserDAO userDao = new UserDAO();
	public FollowingDAO() {
		dbConManagerSingleton = DbConnectionManager.getInstance();
	}
	@Override
	public User get(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<User> getAll() {
		// TODO Auto-generated method stub
		return null;
	}
	public List<User> getAllFollowers(int followingId) {
		ArrayList<User> list = new ArrayList<>();
		
		try {
			ResultSet resultSet = dbConManagerSingleton.excecuteQuery("SELECT follower FROM following WHERE following="+followingId);
			while (resultSet.next()) {
				list.add(userDao.get(resultSet.getInt(1)));
						
				
			}
			dbConManagerSingleton.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	public List<User> getAllFollowing(int followerId) {
		ArrayList<User> list = new ArrayList<>();
		
		try {
			ResultSet resultSet = dbConManagerSingleton.excecuteQuery("SELECT following FROM following WHERE follower="+followerId);
			while (resultSet.next()) {
				list.add(userDao.get(resultSet.getInt(1)));
				
			}
			dbConManagerSingleton.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	
	public boolean save(User follower, User following) {
		PreparedStatement preparedStatement = null;

		try {
			preparedStatement = dbConManagerSingleton.prepareStatement(
											  "INSERT INTO following (follower, following) VALUES (?, ?) ;");		
			preparedStatement.setInt(1, follower.getUserId());
			preparedStatement.setInt(2, following.getUserId());
			preparedStatement.execute();
			dbConManagerSingleton.close();
			return true;
			

		}
		catch ( SQLException e) {
			//e.printStackTrace();
			return false;
		}
		
		
		
	}

	@Override
	public void update(User t, String[] params) {
		// TODO Auto-generated method stub
		
	}

	
	public void delete(User follower,User following) {
		// TODO Auto-generated method stub
		PreparedStatement preparedStatement = null;

		try {

			preparedStatement = dbConManagerSingleton.prepareStatement(
											  "Delete from following where follower=? AND following=?");
			
			preparedStatement.setInt(1, follower.getUserId());
			preparedStatement.setInt(2, following.getUserId());
			preparedStatement.execute();
			
			
			
	
		}
		catch ( SQLException e) {
			e.printStackTrace();
		}
		dbConManagerSingleton.close();
	}
	@Override
	public User save(User t) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void delete(User t) {
		// TODO Auto-generated method stub
		
	}

}
