package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import db.DbConnectionManager;
import model.User;

public class UserDAO implements IDao<User> {
	DbConnectionManager dbConManagerSingleton = null;

	public UserDAO() {
		dbConManagerSingleton = DbConnectionManager.getInstance();
	}

	@Override
	public User get(int id) {
		User user = null;
		try {
			ResultSet resultSet = dbConManagerSingleton.excecuteQuery("SELECT * FROM users WHERE user_id=" + id);
			if (!resultSet.next())
				throw new NoSuchElementException("The point with id " + id + " doesen't exist in database");
			else
				user = new User(id, resultSet.getString(2), resultSet.getString(3), resultSet.getString(4),
						resultSet.getString(5), resultSet.getInt(6), resultSet.getDouble(7), resultSet.getInt(8),
						resultSet.getInt(9));
			dbConManagerSingleton.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		System.out.println("User get(id)" + user.getUserId());
		return user;
	}

	public User get(String email) {
		User user = null;
		try {
			ResultSet resultSet = dbConManagerSingleton
					.excecuteQuery("SELECT * FROM users WHERE email='" + email + "'");
			if (!resultSet.next())
				throw new NoSuchElementException("The user with email " + email + " doesen't exist in database");
			else
				user = new User(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3),
						resultSet.getString(4), resultSet.getString(5), resultSet.getInt(6), resultSet.getDouble(7),
						resultSet.getInt(8), resultSet.getInt(9));
			dbConManagerSingleton.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return user;
	}

	@Override
	public List<User> getAll() {
		ArrayList<User> list = new ArrayList<>();

		try {
			ResultSet resultSet = dbConManagerSingleton.excecuteQuery("SELECT * FROM users ");
			while (resultSet.next()) {
				list.add(new User(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3),
						resultSet.getString(4), resultSet.getString(5), resultSet.getInt(6), resultSet.getDouble(7),
						resultSet.getInt(8), resultSet.getInt(9)));

			}
			dbConManagerSingleton.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public User save(User t) {
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {

			preparedStatement = dbConManagerSingleton.prepareStatement(
					"INSERT INTO users (first_name, last_name, password, email, age, weight, height, heart_rate) "
							+ "VALUES (?, ?, ?, ?,?,?,?,?) RETURNING user_id;");
			preparedStatement.setString(1, t.getFirstName());
			preparedStatement.setString(2, t.getLastName());
			preparedStatement.setString(3, t.getPassword());
			preparedStatement.setString(4, t.getEmail());
			preparedStatement.setInt(5, t.getAge());
			preparedStatement.setDouble(6, t.getWeight());
			preparedStatement.setInt(7, t.getHeight());
			preparedStatement.setInt(8, t.getMaxHeartRate());

			preparedStatement.execute();
			resultSet = preparedStatement.getResultSet();
			resultSet.next();
			int generatedId = resultSet.getInt(1);
			return new User(generatedId, t.getFirstName(), t.getLastName(), t.getPassword(), t.getEmail(), t.getAge(),
					t.getWeight(), t.getHeight(), t.getMaxHeartRate());

		} catch (SQLException e) {
			// e.printStackTrace();
		}

		return new User(0, "fail", null, null, null, 0, 0, 0, 0);
	}

	public User update(User t) {
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			preparedStatement = dbConManagerSingleton.prepareStatement("UPDATE users "
					+ " set first_name=?, last_name=?, password=?, email=?, age=?, weight=?, height=?, heart_rate=? "
					+ "Where email=?" + "returning user_id");
			preparedStatement.setString(1, t.getFirstName());
			preparedStatement.setString(2, t.getLastName());
			preparedStatement.setString(3, t.getPassword());
			preparedStatement.setString(4, t.getEmail());
			preparedStatement.setInt(5, t.getAge());
			preparedStatement.setDouble(6, t.getWeight());
			preparedStatement.setInt(7, t.getHeight());
			preparedStatement.setInt(8, t.getMaxHeartRate());
			preparedStatement.setString(9, t.getEmail());

			preparedStatement.execute();
			resultSet = preparedStatement.getResultSet();
			resultSet.next();
			int generatedId = resultSet.getInt(1);
			return new User(generatedId, t.getFirstName(), t.getLastName(), t.getPassword(), t.getEmail(), t.getAge(),
					t.getWeight(), t.getHeight(), t.getMaxHeartRate());

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return new User(0, "fail", null, null, null, 0, 0, 0, 0);
	}

	@Override
	public void delete(User t) {
		PreparedStatement preparedStatement = null;

		try {
			preparedStatement = dbConManagerSingleton
					.prepareStatement("DELETE FROM users WHERE email='" + t.getEmail() + "'");
			preparedStatement.execute();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void update(User t, String[] params) {
		// TODO Auto-generated method stub

	}

}
