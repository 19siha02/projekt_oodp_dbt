package dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import db.DbConnectionManager;
import model.Activity;
import model.ActivityCategory;

public class CategoryDAO {
	DbConnectionManager dbConManagerSingleton = null;
	PointsDAO pointDao;
	public CategoryDAO() {
		dbConManagerSingleton = DbConnectionManager.getInstance();
	}
	
	public ActivityCategory get(int categoryId) {
		ActivityCategory category = null;
		try {
			ResultSet resultSet = dbConManagerSingleton.excecuteQuery("SELECT * FROM activity_categories where category_id="+categoryId);
			while (resultSet.next()) {
				category = (new ActivityCategory(resultSet.getInt(1), resultSet.getString(2),  resultSet.getInt(3))
						);
				
			}
			dbConManagerSingleton.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return category;
	}

	
	public List<ActivityCategory> getAll() {
		ArrayList<ActivityCategory> list = new ArrayList<>();
		
		try {
			ResultSet resultSet = dbConManagerSingleton.excecuteQuery("SELECT * FROM activity_categories ");
			while (resultSet.next()) {
				list.add(new ActivityCategory(resultSet.getInt(1), resultSet.getString(2),  resultSet.getInt(3))
						);
				
			}
			dbConManagerSingleton.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	
	public void update(Activity t, String[] params) {
		// TODO Auto-generated method stub
		
	}

	
	public void delete(Activity t) {
		// TODO Auto-generated method stub
		
	}

}
